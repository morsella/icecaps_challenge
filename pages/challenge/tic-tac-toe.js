import React, { useState, useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';

const rowStyle = {
  display: 'flex'
}

const squareStyle = {
  'width':'60px',
  'height':'60px',
  'backgroundColor': '#ddd',
  'margin': '4px',
  'display': 'flex',
  'justifyContent': 'center',
  'alignItems': 'center',
  'fontSize': '20px',
  'color': 'white'
}

const boardStyle = {
  'backgroundColor': '#eee',
  'width': '208px',
  'alignItems': 'center',
  'justifyContent': 'center',
  'display': 'flex',
  'flexDirection': 'column',
  'border': '3px #eee solid'
}

const containerStyle = {
  'display': 'flex',
  'alignItems': 'center',
  'flexDirection': 'column'
}

const instructionsStyle = {
  'marginTop': '5px',
  'marginBottom': '5px',
  'fontWeight': 'bold',
  'fontSize': '16px',
}

const buttonStyle = {
  'marginTop': '15px',
  'marginBottom': '16px',
  'width': '80px',
  'height': '40px',
  'backgroundColor': '#8acaca',
  'color': 'white',
  'fontSize': '16px',
}

const Square = (props) => {
    return (
      <div
        className="square"
        style={squareStyle} onClick={props.onClick}>
      </div>
    );
}

const Board = () => {
    const [count, setCount] = useState(0);
    const [playerX, setPlayerX] = useState([]);
    const [playerY, setPlayerY] = useState([]);
    const [winner, setWinner] = useState('None');

    const squareRef = useRef(null);

    useEffect(() => {
       if(playerX.length >= 3){
            checkRow(playerX);
       }
       if(playerY.length >= 3){
            checkRow(playerY);
        }
    }, [playerX, playerY])

    const checkRow = (player) => {
        for(let i=0; i<player.length; i++){
           const matchX = player.filter(item => item.x === player[i].x );
           const matchY = player.filter( item => item.y === player[i].y );
           if(matchX.length === 3 || matchY.length === 3) {
                setWinner((count-1) % 2 === 0? 'X': 'O');
            } else {
                checkDiagonal(player);
            }
        }
    }

    const checkDiagonal = (player) => {
        const minY = player.reduce((min, p, i) => p.y < min ? p.y : min, player[0].y);
        const minX = player.reduce((min, p, i) => p.x < min ? p.x : min, player[0].x);

        const maxY = player.reduce((max, p, i) => p.y > max ? p.y : max, player[0].y);
        const maxX = player.reduce((max, p, i) => p.x > max ? p.x : max, player[0].x);

        const center = player.find(p => (p.x !== minX && p.y !== minY && p.x !== maxX && p.y !== maxY));
        if(center){
            const match1Top = player.find(p => (p.x < center.x && p.y < center.y));
            const match1Bottom = player.find(p => (p.x > center.x && p.y > center.y));

            const match2Top = player.find(p => (p.x > center.x && p.y < center.y));
            const match2Bottom = player.find(p => (p.x < center.x && p.y > center.y));

            if((match1Top && match1Bottom) || (match2Top && match2Bottom)) {
                setWinner((count-1) % 2 === 0? 'X': 'O');
            }
        }
    }
    const setPosition = (event) => {
        const player = { x: event.target.offsetLeft, y: event.target.offsetTop };
        if( winner !== 'None' ) return;
        if( count % 2 === 0 ){
            setPlayerY(playerY => [...playerY, player]);
            event.target.innerHTML = 'X';
        } else { 
            setPlayerX(playerX => [...playerX, player]);
            event.target.innerHTML = 'O';
        }
        setCount( count + 1 );
    }
    const resetGame = () => {
        const rows = squareRef.current.children;
        const rowsLenght= squareRef.current.children.length;

        for(let i=0; i < rowsLenght; i++){
            for(let j=0; j < rowsLenght; j++){
                rows[i].children[j].innerHTML = '';
            }
        }
        setCount(0);
        setPlayerX([]);
        setPlayerY([]);
        setWinner('None');
    }
    const renderBoard = (rows) => {
        let squares = [];
        let board = [];
        for(let i = 1; i <= rows; i++){
            squares.push(<Square onClick={setPosition} key={i} />);
        }
        for(let j = 1; j <= rows; j++){
            board.push(<div className="board-row" style={rowStyle} key={j}>{squares}</div>);
        }
        return (board);
    }
    return (
      <div style={containerStyle} className="gameBoard">
        <div className="status" style={instructionsStyle}>{ count < 9 && winner === 'None' ? 'Next player: ' + (count % 2 === 0 ? 'X': 'O') : 'Game Over'}</div>
        <div className="winner" style={instructionsStyle}>Winner: {winner}</div>
        <button style={buttonStyle} onClick={resetGame}>Reset</button>
        <p>{playerY.length} {playerX.length}</p>
        <div style={boardStyle} ref={squareRef}>
            {renderBoard(3)}
        </div>
      </div>
    );
}

const Game = () => {
    return (
      <div className="game">
        <div className="game-board">
          <Board />
        </div>
      </div>
    );
}

export default Game;

// ReactDOM.render(
//   <Game />,
//   document.getElementById('root')
// );