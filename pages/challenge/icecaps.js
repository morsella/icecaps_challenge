import React, { useState, useRef, useEffect } from 'react';

const style = {
  table: {
    borderCollapse: 'collapse'
  },
  tableCell: {
    border: '1px solid gray',
    margin: 0,
    padding: '5px 10px',
    width: 'max-content',
    minWidth: '250px'
  },
  form: {
    container: {
      padding: '20px',
      border: '1px solid #F0F8FF',
      borderRadius: '15px',
      width: 'max-content',
      marginBottom: '40px'
    },
    inputs: {
      marginBottom: '5px',
      width: '300px'
    },
    submitBtn: {
      marginTop: '10px',
      padding: '10px 15px',
      border:'none',
      backgroundColor: 'lightseagreen',
      fontSize: '14px',
      borderRadius: '5px'
    }
  }
}

function CustomerForm(props) {
  return (
    <form onSubmit={e => props.updateList(e)} style={style.form.container}>
      <label>Full name:</label>
      <br />
      <input 
        style={style.form.inputs}
        className='userFullname'
        name='userFullname' 
        value={props.contactForm.fullName?.value}
        onChange={event=>props.inputChange(event, 'fullName')} 
        {...props.contactForm.fullName?.elementConfig}
      />
      <br/>
      <label>Email:</label>
      <br />
      <input 
        style={style.form.inputs}
        className='userEmail'
        name='userEmail' 
        value={props.contactForm.email?.value} 
        onChange={event=>props.inputChange(event, 'email')} 
        {...props.contactForm.email?.elementConfig}
      />
      <br />
      <label>Date of Birth:</label>
      <br />
      <input
        style={style.form.inputs}
        className='birthDate' 
        name='birthDate' 
        type='text'
        value={props.contactForm.birthDate?.value} 
        onChange={event => props.inputChange(event, 'birthDate')}
        onFocus={e => e.target.type = 'date'}
        {...props.contactForm.birthDate?.elementConfig}
      />
      <br />
      <label>Select Donations Amount:</label><p>&#36; {props.contactForm.amountRange?.value}</p>
      <input 
        style={style.form.inputs} 
        name="amountRange"
        value={props.contactForm.amountRange?.value}
        onChange={e => props.inputChange(e, 'amountRange')}
        {...props.contactForm.amountRange?.elementConfig} />
      <br />
      <label>
      <input 
        name="gdpr"
        type='checkbox'
        onChange={e => props.inputChange(e, 'gdpr')}
        /> GDPR</label>
      <br />
      <input 
        style={style.form.submitBtn} 
        className='submitButton'
        type='submit' 
        value='Subscribe User' 
      />
    </form>
  )
}

function InformationTable(props) {

    let list = [...props.contactList];

    list = list.map((l, i) => {
        return (
            <tr key={i}>
                <td>{l.fullName}</td>
                <td>{l.email}</td>
                <td>{l.birthDate}</td>
                <td>{l.amountRange}</td>
            </tr>
        );
    });

  return (
    <table style={style.table} className='informationTable'>
      <thead> 
        <tr>
          <th style={style.tableCell}>Full name</th>
          <th style={style.tableCell}>Email</th>
          <th style={style.tableCell}>Date of Birth</th>
          <th style={style.tableCell}>Donation Amount</th>
        </tr>
        {list}
      </thead> 
    </table>
  );
}

function Application(props) {
    const [ contactList, setContactList ] = useState([]);
    const [ contactForm, setContactForm ] = useState({
        fullName:{
            elementConfig: {
                type: 'text',
                placeholder: 'Coder Byte'
            },
            value: '',
            validation:{
                required: true,
                isString: true
            },
            valid: false
        },
        email:{
            elementConfig: {
                type: 'text',
                placeholder: 'coderbyte@coderbyte.com'
            },
            value: '',
            validation:{
                required: true
            },
            valid: false
        },
        birthDate:{
            elementConfig: {
                placeholder: '07-22-1986'
            },
            value: '',
            validation:{
                required: true
            },
            valid: false
        },
        amountRange:{
            elementConfig:{
                type:'range',
                placeholder: '50',
                min: '1',
                max: '100'
            },
            value: '50',
            validation: {
                required: true
            },
            valid: false
        },
        gdpr:{
            checked: false,
            validation: {
                required: true
            },
            valid: false
        }
    });
    const inputChangedHandler = (event, controlName) => {
      let object;
        if(controlName === 'gdpr'){
          object = updateObject(contactForm[controlName], {
            checked: event.target.checked,
            valid: event.target.checked
          })
        } else {
          object = updateObject(contactForm[controlName], {
            value: event.target.value,
            valid: checkvalidity(event.target.value, contactForm[controlName].validation)
          })
        }
        let updatedControls = updateObject(contactForm, {
          [controlName]: object
        });
        if(updatedControls[controlName].valid){
            setContactForm(updatedControls);
        }
    };    
    const checkvalidity = (value, rules) => {
        const numeric = /^[0-9]+$/;
        const alphabetic = /^[A-Za-z ]+$/;
        let valid = true;
        if(!rules){
            return true;
        }
        if(rules.required) {
            valid = value.trim() !== '';
        }
        if(rules.isNumeric){
            valid = !!value.match(numeric);
        }
        if(rules.isString){
            valid = !!value.match(alphabetic);
        }

        return valid;
    }
    const updateObject = (oldObject, updatedProperties) => {
        return {
            ...oldObject,
            ...updatedProperties
        };
    };
    const updateList = (e) => {
        e.preventDefault();
        const contact = {
            fullName: contactForm.fullName.value ? contactForm.fullName.value : contactForm.fullName.elementConfig.placeholder,
            email: contactForm.email.value ? contactForm.email.value : contactForm.email.elementConfig.placeholder,
            birthDate: contactForm.birthDate.value ? contactForm.birthDate.value : contactForm.birthDate.elementConfig.placeholder,
            amountRange: contactForm.amountRange.value,
            gdpr: contactForm.gdpr.value
        };
        setContactList([...contactList, { fullName: contact.fullName, email: contact.email, birthDate: contact.birthDate, amountRange: contact.amountRange, gdpr: contact.gdpr }]);
    }


  return (
    <section>
      <CustomerForm contactForm={contactForm} inputChange={inputChangedHandler} updateList={updateList} />
      <InformationTable contactList={contactList} />
    </section>
  );
}

export default Application;