import React, { useState } from 'react';
import ReactDOM from 'react-dom';

class Toggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isOn: false,  count: 0 };
  }
  render() {
    return (
      <div>
      <p>You clicked {this.state.count} times</p>
      <button
      onClick={() => this.setState({ count: this.state.count + 1, isOn: !this.state.isOn })}>{this.state.isOn? 'ON': 'OFF'}</button></div>
    );
  }
}

export default Toggle;
// ReactDOM.render(
//   <Toggle />,
//   document.getElementById('root')
// );