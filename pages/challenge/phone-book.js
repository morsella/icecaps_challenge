import React, { useState } from 'react';
import ReactDOM from 'react-dom';

const style = {
  table: {
    borderCollapse: 'collapse'
  },
  tableCell: {
    border: '1px solid gray',
    margin: 0,
    padding: '5px 10px',
    width: 'max-content',
    minWidth: '150px'
  },
  form: {
    container: {
      padding: '20px',
      border: '1px solid #F0F8FF',
      borderRadius: '15px',
      width: 'max-content',
      marginBottom: '40px'
    },
    inputs: {
      marginBottom: '5px'
    },
    submitBtn: {
      marginTop: '10px',
      padding: '10px 15px',
      border:'none',
      backgroundColor: 'lightseagreen',
      fontSize: '14px',
      borderRadius: '5px'
    }
  }
}

function PhoneBookForm(props) {
  return (
    <form onSubmit={e => props.updateList(e)} style={style.form.container}>
      <label>First name:</label>
      <br />
      <input 
        style={style.form.inputs}
        className='userFirstname'
        name='userFirstname' 
        value={props.contactForm.firstName.value} 
        onChange={event=>props.inputChange(event, 'firstName')} 
        {...props.contactForm.firstName.elementConfig}
      />
      <br/>
      <label>Last name:</label>
      <br />
      <input 
        style={style.form.inputs}
        className='userLastname'
        name='userLastname' 
        value={props.contactForm.lastName.value} 
        onChange={event=>props.inputChange(event, 'lastName')} 
        {...props.contactForm.lastName.elementConfig}
      />
      <br />
      <label>Phone:</label>
      <br />
      <input
        style={style.form.inputs}
        className='userPhone' 
        name='userPhone' 
        value={props.contactForm.phoneNumber.value} 
        onChange={event=>props.inputChange(event, 'phoneNumber')} 
        {...props.contactForm.phoneNumber.elementConfig}
      />
      <br/>
      <input 
        style={style.form.submitBtn} 
        className='submitButton'
        type='submit' 
        value='Add User' 
      />
    </form>
  )
}

function InformationTable(props) {

    let list = [...props.contactList];

    const sortedList = list.sort((a,b) => { 
        return  a.lastName < b.lastName ? -1 : 1; 
    });

    list = sortedList.map((l, i) => {
        return (
            <tr key={i}>
                <td>{l.firstName}</td>
                <td>{l.lastName}</td>
                <td>{l.phoneNumber}</td>
            </tr>
        );
    });

  return (
    <table style={style.table} className='informationTable'>
      <thead> 
        <tr>
          <th style={style.tableCell}>First name</th>
          <th style={style.tableCell}>Last name</th>
          <th style={style.tableCell}>Phone</th>
        </tr>
        {list}
      </thead> 
    </table>
  );
}

function Application(props) {
    const [ contactList, setContactList ] = useState([]);
    const [ contactForm, setContactForm ] = useState({
        firstName:{
            elementConfig: {
                type: 'text',
                placeholder: 'Coder'
            },
            value: '',
            validation:{
                required: true,
                isString: true
            },
            valid: false
        },
        lastName:{
            elementConfig: {
                type: 'text',
                placeholder: 'Byte'
            },
            value: '',
            validation:{
                required: true,
                isString: true
            },
            valid: false
        },
        phoneNumber:{
            elementConfig: {
                type: 'tel',
                placeholder: '8885559999'
            },
            value: '',
            validation:{
                required: true,
                isNumeric: true,
                minLength: 10,
                maxLength: 10
            },
            valid: false
        }
    });
    const fistLetterUpperCase = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    const inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(contactForm, {
          [controlName]: updateObject(contactForm[controlName], {
            value: event.target.value,
            // Todo valid: checkvalidity(
            // will take e.target as a value, form[control].validation  as rules... 
            // 
            //)  
          })
        });
        setContactForm(updatedControls);
        if(updatedControls[controlName].valid){
            props.setCustomerForm(updatedControls[controlName].value, controlName);
        }
    };    
    const checkvalidity = (value, rules) => {
        let valid = true;
        if(!rules){
            return true;
        }

        if(rules.required) {
            valid = value.trim() !== ''
        }
        return valid;
    }
    const updateObject = (oldObject, updatedProperties) => {
        return {
            ...oldObject,
            ...updatedProperties
        };
    };
    const updateList = (e) => {
        e.preventDefault();
        const contact = {
            firstName: contactForm.firstName.value ? fistLetterUpperCase(contactForm.firstName.value) : "Coder",
            lastName: contactForm.lastName.value ? fistLetterUpperCase(contactForm.lastName.value) : "Byte",
            phoneNumber: contactForm.phoneNumber.value ? contactForm.phoneNumber.value : "8885559999"
        };
        setContactList([...contactList, { firstName: contact.firstName, lastName: contact.lastName, phoneNumber: contact.phoneNumber }]);
    }


  return (
    <section>
      <PhoneBookForm contactForm={contactForm} inputChange={inputChangedHandler} updateList={updateList} />
      <InformationTable contactList={contactList} />
    </section>
  );
}

export default Application;