import { useState } from 'react';
import Router from 'next/router';
import useRequest from '../../hooks/use-request';

const SignupComponent = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [organizationName, setCompany] = useState('');
    const { doRequest, errors } = useRequest({
        url: '/api/users/signup',
        method: 'post',
        body: {
          email,
          password,
          organizationName
        },
        onSuccess: () => Router.push('/')
      });

    const onSubmit = async event => {
        event.preventDefault();
        console.log(email, password, organizationName);
        
        await doRequest();
    };
    return (
        <form onSubmit={onSubmit}>
            <h1>Sign Up</h1>
            <div className="form-group">
                <label>Email:</label>
                <input 
                    value={email} 
                    onChange={e => setEmail(e.target.value)}
                    type="text" className="form-control" />
                <label>Password:</label>
                <input 
                    type="password" 
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    className="form-control" />
                <label>Company:</label>
                <input 
                    type="text" 
                    value={organizationName}
                    onChange={e => setCompany(e.target.value)}
                    className="form-control" />
            </div>
            {errors}
            <button className="btn btn-primary">Sign Up</button>
        </form>
    );
};

export default SignupComponent;