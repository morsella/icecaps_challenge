import axios from 'axios';

const buildBase = ({ req }) => {
    if(typeof window === 'undefined') {
        //sever side
        return axios.create({
            baseURL: 'http://ingress-nginx-controller.ingress-nginx.svc.cluster.local',
            headers: req.headers
        });
    } else {
        //browser side
        return axios.create({
            baseURL: '/'
        });
    }
};

export default buildBase;